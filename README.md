- [[Mann Labs PKMS post 2022-05-27]]
- [[HI and PKMS draft]]
---
## How Smart Glasses Can Be Used for Personal Knowledge Management
Author: Adrian Papineau
Date written: 2022-06-04

![Image](https://i.imgur.com/bml41hg.jpg)
#### Introduction
One of the goals of knowledge management is to become closer to our information by forming meaningful connections between concepts to establish a network of knowledge that extends beyond our physical self. The digital or hand written notes we make become an external memory network, commonly refereed to as a "second brain". Everyone has some form of external memory network. Our mobile phones are one example. To look up someone's email address, the user knows to unlock the phone, click on the "contacts" app icon, and type in the name of the desired contact to retrieve the information they're looking for. This is an example of an external memory path, although it takes a little bit of attention, effort, and time to reach the information. This technology and methodology can be improved on to reach the goal of recalling information easier and faster with the aid of wearable devices such as AR/XR smart glasses, which can immediately bring information into your field of vision. Ultimately, a synergistic system between human and computer can be created for our own benefit. They can work together; the computer performs the tasks that it is better at, while the human performs the tasks that he/she is better at [2]. This synergy between human and computer is the basis of humanistic intelligence. 

#### Types of Realities
AR/XR smart glasses can function in a variety of ways, so it is important to differentiate the types of computer-aided realities when talking about them. Virtual reality (VR) blocks out the real world completely, and creates a digital environment for the user to experience [11]. There is also pass-through VR, which is close to AR. Augmented reality (AR) by itself is just an overlay of digital information onto the real world [9]. Mixed reality (MR) is commonly known to be a type of augmented reality in which digital objects can interact with the real world [12]. Mediated Realty (XYR) could be considered mixed reality, but specifically deals with mediating reality, such as filtering out advertisements or censorship [4]. Quantimetric reality (QR) is multidimensional and multisensory, meaning that things like heart rate, blood pressure, or BCI data is also used as part of influencing the reality. Both XR and All-R refer to a generalized term for all types of realities [4]. A graphical depiction of how the reality types are related is shown in figure 1. 

![Image](https://i.imgur.com/gVX5txT.png)
Figure 1: A graph showing the different types of realities and how they are related. 

#### Modern Smart Glasses
Companies such as Vuzix, Xiaomi, and Qualcomm are working towards the development of practical smart glasses right now. There are headsets that already exist that allow users to experience mixed reality. Of course, aesthetics is an important factor to consider in the development of smart glasses. In order for smart glasses to become mainstream devices, they need to look like regular glasses in order to be socially acceptable [1]. Soon, there will be smart glasses on the market that fit this criteria. North Focals is an example of smart glasses that were socially acceptable, but are no longer in production. The company Vuzix has publicly announced the development of the Shield smart glasses, claimed to be a fully functional wearable computer running on Android that has a look and feel to regular glasses, although they are marketed towards industrial companies as they are also safety glasses, hence the name. The development of a personal knowledge system application for it may be possible, although it is currently unknown if they will release the software development kits necessary to build it. There are not many apps for smart glasses currently that are made to manage personal knowledge, although there is interest in the development of such apps. The open-source Wearable Information System (WIS) application platform developed by Cayden Pierce has personal knowledge management components and works well for the Vuzix Blade, the Shield's predecessor [10]. 

#### Sousveillance 
There is a lot of controversy regarding if smart glasses should have cameras or not [5]. Cameras unlock a very large amount of potential for smart glasses. The ability for smart glasses to record video (instead of just processing it) isn't the main role of the camera(s), however. With cameras, digital information can interact with objects in the real world. Image processing and machine learning can be used for object recognition, facial recognition, and spatial awareness. In public environments when stores use video cameras to record us while we shop, it is commonly known as surveillance (oversight). When individuals are doing the recording, it is known as sousveillance (undersight) [4]. Time will tell if the discomfort people have with cameras on smart glasses will wear off.
``
#### Personal Knowledge Management
There are plenty of desktop and mobile programs right now that are all focusing on personal knowledge management such as Logseq, Obsidian, or Roam Research. A specialised app could be made for smart glasses that work *alongside* your desktop app of choice to access your personal knowledge base. Another aspect of PKM systems is bi-directional linking. In most systems, wikilinks are used to link relevant information with the goal being to create paths of knowledge. Bi-directional linking means that the source block containing the link will show up in the linked file, so that you can navigate backwards through your information. Maps of content are often made to find things easier. They are like a table of contents that allow you to jump from note to note in order to create shorter paths to access information you may need quickly. These paths become a network of information that can be accessed using smart glasses. 

Popular PKM Methods such as the Zettelkasten method allow you to manage and retain information effectively. In summary with this method, you first write *fleeting notes*, which are rough notes that can be in any format or even written on a napkin; the point is to just get the information down. After this, you write formatted *permanent notes* based on your fleeting notes. It can be in any format, as long as it is in your own words. Putting things into your own words strengthens the memory of your information dramatically. 

At a basic level, you would at least need two functions in order to be considered a personal knowledge management system: the ability to write to files, and the ability to retrieve and read files. Referencing information on the fly could be very fast and simple with the aid of smart glasses. Using a wearable one handed keyboard such as the Twiddler3 [8], you could quickly search for a topic of interest in your digital personal knowledge base, and have it displayed right in front of you in a matter of seconds. 

#### Future of PKM
 > Perhaps intelligent eyeglasses of the future will anticipate what is important to us and select the sampling rate accordingly to reveal salient details [3] (Mann, 1997)

Relationships between notes/concepts can be extracted using natural language processing and machine learning. This will allow us to see our knowledge base from different perspectives. It will also allow features that go beyond current knowledge management system methods, such as logical problem solving. Because smart glasses offer the ability to see information immediately as it appears, psychological methods such as using salience to recall information could be used. Machine learning (and ultimately artificial intelligence) can help us in many aspects of life. There is a difference between this (personal assistance), to personal knowledge management and life management. With personal assistance, the computer assists you with things like face recognition and identification. With personal knowledge management, you are in control of your own information. potentially, personal assistance can use the information from your personal knowledge base to better understand and assist you. Personal knowledge management can include any information type such as digital notes, written notes, multi-media, etc...

There are at least two main approaches to how a wearable PKM app could be made; using strictly an AR overlay, or using MR to have your information interact with the real world. There are limitations that current smart glasses have that would prevent an MR platform being developed on them, which is why an AR overlay user interface might be the preferred option for now. There is a trade off that smart glasses have where size, comfort, and style generally mean less technical features and performance. The devices that are capable of mixed reality personal knowledge management are currently bulky headsets that are not realistic to wear everyday. There are advantages and disadvantages for both AR and MR. With an AR overlay of information, it has a greater potential to block out your environment, while MR glasses can anchor text to points in space that do not take away from your peripheral vision. While using an AR interface, there should be a limit to the amount of information occupying your field of view so that way there is still a good balance between digital information and your environment, so that you can shift your attention immediately to the real world if you needed to, such as if a bike was riding towards you and you needed to get out of the way. 

![Image](https://i.imgur.com/18nlgOw.png)
Figure 2: An example user experience that is primarily an AR overlay. 

It is within the realm of possibilities that a smart glasses program/platform can become so sophisticated that human and technology become one feedback loop; the device relies on data from the human to perform it's computations, and the user relies on the computer to help him/her think. Machine learning could be used based on the objects in your environment to determine situational context so that it can predict what to show you that will be useful and relevant to you. 

#### Life Management
Tasks and reminders can be situation-based. For example, 'make a reminder to buy flip flops next time I am at the store'. Data from camera(s) can identify the setting you are in with object recognition. Another example may 'show me all outstanding tasks once I am relaxed'. If heart rate is monitored with the device, it can tell when you are relaxed enough to see your outstanding tasks. Things like monitoring your daily agenda or viewing your calendar can all be done on smart glasses. 

Data collected externally from smart glasses could potentially be shared between users. Information imputed by the user should never be shared unless done so by the user. Increasing the amount of data available in your environment allows the smart glasses to know more about what's going on around you, which can give the user better suggestions and notifications. An example of how this could work is if you were about to go to a gas station, and your system warns you that the station may be closed, since someone else's smart glasses seen that the gas station was closed.

The singularity concept that says that one day, humans will bond with technology and become basically cyborgs. We are all almost there already, because we use our cell phones to manage our knowledge otherwise. The difference between using smart phones and smart glasses is that with smart glasses, the information transfer from device to human is seamless, whereas with a cell phone, it takes a longer amount of time to get your phone out of your pocket to look at it. 

#### Conclusion

Smart glasses offer a seamless transfer of information from computer to user, which could be an advantage of using smart glasses with a personal knowledge management system. Personal knowledge management in particular can be improved with the aid of smart glasses; searching and retrieving relevant information, writing notes on-the-fly, or getting inspired are some uses for PKM with smart glasses. Technology can help us form external connections to help navigate and retain our knowledge, and there are methods that help with this. Allowing technology to tackle our weaknesses for us may make us be better humans by solving some of life's problems.

### References 
[1] Rauschnabel A. Hein D. He J. Ro Y. Rawashdeh S. Krulikowski B. (2016). Fashion or Technology? A Fashnology Perspective on the Perception and Adoption of Augmented Reality Smart Glasses [Link](https://fis.uni-bamberg.de/bitstream/uniba/40795/1/icom2016_0021se_A3b.pdf)

[2] Mann S. (1998). Humanistic Computing: “WearComp” as a New Framework and Application for Intelligent Signal Processing [Link](http://wearcam.org/hi.pdf)

[3] Mann S. (1997). Wearable Computing: A First Step Toward Personal Imaging [Link](http://wearcam.org/ieeecomputer.pdf)

[4] Mann S. Havens J. Iorio J. Yuan Y. Furness T. (2018). All Reality: Values, taxonomy, and continuum, for Virtual, Augmented, eXtended/MiXed (X), Mediated (X,Y), and Multimediated Reality/Intelligence [Link](http://wearcam.org/all.pdf)

[5] Hofmann B. Haustein D Landeweerd L. (2016). Smart-glasses: exposing and elucidating the ethical issues [Link](https://www.duo.uio.no/bitstream/handle/10852/59746/Smart-glasses+Exposing+and+elucidating+the+ethical+issues+Hofmann+et+al+2017.pdf?sequence=2)

[6] Aman E. (2021). A Comprehensive Review of Smart Glasses Technology- Future of Eyewear [Download link](https://turcomat.org/index.php/turkbilmat/article/download/669/467/1167)

[7] Miller F. (2018). Organising knowledge with multi-level content: Making knowledge easier to understand, remember and communicate [Link](https://www.francismiller.com/organising_knowledge_paper.pdf)

[8] Ok A. Basoglu N. Tugrul D. (2015). Exploring the Design Factors of Smart Glasses

[9] Starner T. Mann S. Bradley R. Headley J. Russel B. Levine J. Pentland A. (1995). Wearable Computing and Augmented Reality

[10] Pierce C. Mann. S. (2021). Wearable Affective Memory Augmentation

[11] Wired. (2019). Get Ready to Hear a Lot More About 'XR'" ISSN 1059-1028. 

[12] Milgram, Paul & Kishino, Fumio. (1994). A Taxonomy of Mixed Reality Visual Displays. IEICE Trans. Information Systems. vol. E77-D, no. 12. 1321-1329.
